let isDarkMode = true
function darkMode(){
    if(isDarkMode){
        isDarkMode = !isDarkMode
        document.querySelector('html').setAttribute('data-bs-theme', 'light')
        document.getElementById('darkMode').innerHTML = '🌙 Dark Mode'
        document.getElementById('darkMode').classList.remove("btn-dark")
        document.getElementById('darkMode').classList.add("btn-light")
    } else {        
        isDarkMode = !isDarkMode
        document.querySelector('html').setAttribute('data-bs-theme', 'dark')
        document.getElementById('darkMode').innerHTML = '🌞 Light Mode'
        document.getElementById('darkMode').classList.remove("btn-light")
        document.getElementById('darkMode').classList.add("btn-dark")
    }
}

let numImg = 0
let isGenerating = false

function addImgToPage(b64str){
    if(numImg % 3 == 0){
        document.getElementById('imgDisplay').innerHTML += `<div class="row align-items-start">
            <div id="${numImg}" class="col"></div>
            <div id="${numImg+1}" class="col"></div>
            <div id="${numImg+2}" class="col"></div>
            </div>`
        document.getElementById(numImg).innerHTML = `<img src="${b64str}" alt="Image #${numImg+1}" />`
        ++numImg
    } else {
        document.getElementById(numImg).innerHTML = `<img src="${b64str}" alt="Image #${numImg+1}" />`
        ++numImg
    }
}

let photosuccess = false
let debugRounds = 0

function logger(){console.log("examination")}

async function checkImg(b64str){
    try{
        // if(debugRounds > 5){debugRounds = 0; console.log('er');return true}
        console.log('checking image')
        let test = new Image()
        test.onerror = () => {console.log('loadimg err');photosuccess = false;return}
        test.onload = () => {console.log('img sez loaded');photosuccess = true;return}
        test.src = b64str
        await new Promise(resolve => setTimeout(resolve, 1000));
        console.log('test',b64str.length)
        if(photosuccess){console.log('awaited n true');return true}else{console.log("early and false");return false}
        // document.getElementById('testBox').innerHTML = `<img src="${b64str}" />`
        // document.getElementById('testBox').appendChild = test
        // console.log('should contain somth')
    } catch(err){
        console.log('checkimg err', err)
    }
}

function getB64String(){
    const choicesArr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', 
        '8', '9', '+', '/'];
    // let output = "data:image/jpeg;base64,"
    // let output = 'data:image/png;base64,'
    let output = ''
    // let output = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=='
    let length = Math.floor(Math.random() * 65000)
    while(length % 24 != 0){
    // while(length % 16 != 0){
        console.log('len retry')
        length = Math.floor(Math.random() * 65000)
    }
    for(let i=0; i<length; i++){
        output += choicesArr[Math.floor(Math.random() * (64))]
    }

    let hexString = '89 50 4E 47 0D 0A 1A 0A ' // PNG Header
    hexString += '00 00 00 0D 49 48 44 52 00 00 00 01 00 00 00 01 08 02 00 00 00 90 77 53 DE ' //PNG IDHR
    hexString += '00 00 00 0C 49 44 41 54 08 D7 63 F8 CF C0 00 00 03 01 01 00 18 DD 8D B0'     //PNG IDAT
    hexString += '00 00 00 00 49 45 4E 44 AE 42 60 82' // PNG IENDB

    //lampcore.ru
    hexString = '89 50 4E 47 0D 0A 1A 0A 00 00 00 0D 49 48 44 52 00 00 00 80 00 00 00 7F 08 03 00 00 00 10 24 3A 35'
    hexString += ""

    var base64String = Buffer.from(hexString, 'hex').toString('base64')

    output = "data:image/png;base64," + output
    // console.log("wrote b64", output)
    return output
}

async function generateImg(){
    isGenerating = true
    let generatedSuccessfully = false
    while(!generatedSuccessfully){
    // for(let i=0; i<15;i++){ 
        let imgStr = getB64String()
        drawOn()
        if(await checkImg(imgStr)){
            console.log('success')
            generatedSuccessfully = true
            isGenerating = false
            addImgToPage(imgStr)
        } else {
            console.log('unsuccessful genImg')
        }
    }
}

function drawOn(){
    let canvas = document.getElementById('can')
    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "#000";
    ctx.fillRect(0,0,1200,850)
    const reps = Math.floor(Math.random() * 128)
    // ctx.fillRect(Math.floor(Math.random() * 600), Math.floor(Math.random() * 600), Math.floor(Math.random() * 600), Math.floor(Math.random() * 600));
    for(let i=0; i<reps; ++i){
        console.log('strokin')
        // ctx.strokeStyle = ctx.createConicGradient(Math.floor(Math.random() * 360), Math.floor(Math.random() * 600), Math.floor(Math.random() * 600))
        const colorOpts = '0123456789ABCDEF'
        ctx.strokeStyle = `#${colorOpts[Math.floor(Math.random() * 17)]}${colorOpts[Math.floor(Math.random() * 17)]}${colorOpts[Math.floor(Math.random() * 17)]}`
        ctx.beginPath();
        ctx.moveTo(Math.floor(Math.random() * 600), Math.floor(Math.random() * 600));
        ctx.lineTo(Math.floor(Math.random() * 600), Math.floor(Math.random() * 600));
        ctx.lineTo(Math.floor(Math.random() * 600), Math.floor(Math.random() * 600));
        ctx.lineTo(Math.floor(Math.random() * 600), Math.floor(Math.random() * 600));
        ctx.lineTo(Math.floor(Math.random() * 600), Math.floor(Math.random() * 600));
        ctx.lineTo(Math.floor(Math.random() * 600), Math.floor(Math.random() * 600));
        ctx.lineTo(Math.floor(Math.random() * 600), Math.floor(Math.random() * 600));
        ctx.lineTo(Math.floor(Math.random() * 600), Math.floor(Math.random() * 600));
        ctx.closePath();
        ctx.stroke();
    }
}

const dispWidth = 1200
const dispHeight = 850
let curFrame = []
let frames = 999
let reps = 0
let runnin

function drawOnRepeat(){
    runnin = setInterval(drawingRepeatedly, 40)
}

function quitDrawingPls(){
    clearInterval(runnin)
}

function drawingRepeatedly(){
    let canvas = document.getElementById('can')
    const ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "#000";
    ctx.fillRect(0,0,dispWidth,dispHeight)
    if(frames < 120){
        // modify existing
        let j = 0
        for(let i=0; i<reps; ++i){
            ctx.strokeStyle = curFrame[j]
            ++j
            ctx.beginPath();
            ctx.moveTo(curFrame[j][0],curFrame[j][1]);
            j++
            curFrame[j][0]+=2
            curFrame[j][1]+=2
            ctx.lineTo(curFrame[j][0],curFrame[j][1])
            ++j
            // for(let l = 0; l<7;l++){
            //     let currChange = Math.floor(Math.random() * 4)
            //     if(currChange==0){curFrame[j][0]-=2;curFrame[j][1]-=2}
            //     if(currChange==1){curFrame[j][0]-=2;curFrame[j][1]-=2}
            //     if(currChange==2){curFrame[j][0]+=2;curFrame[j][1]+=2}
            //     if(currChange==3){curFrame[j][0]-=2;curFrame[j][1]-=2}
            //     ctx.lineTo(curFrame[j][0],curFrame[j][1])
            //     j++
            // }
            // j++
            curFrame[j][0]-=2
            curFrame[j][1]+=2
            ctx.lineTo(curFrame[j][0],curFrame[j][1])
            ++j
            curFrame[j][0]+=2
            curFrame[j][1]-=2
            ctx.lineTo(curFrame[j][0],curFrame[j][1])
            ++j
            curFrame[j][0]-=2
            curFrame[j][1]-=2
            ctx.lineTo(curFrame[j][0],curFrame[j][1])
            ++j
            curFrame[j][0]-=2
            curFrame[j][1]+=2
            ctx.lineTo(curFrame[j][0],curFrame[j][1])
            ++j
            curFrame[j][0]+=2
            curFrame[j][1]-=2
            ctx.lineTo(curFrame[j][0],curFrame[j][1])
            ++j
            curFrame[j][0]+=2
            curFrame[j][1]+=2
            ctx.lineTo(curFrame[j][0],curFrame[j][1])
            ++j
            // curFrame[j][0]-=2
            // curFrame[j][1]-=2
            // ++j
            ctx.closePath();
            ctx.stroke();
        }    
        ++frames
    } else {
        // generate new
        curFrame = []
        reps = Math.floor(Math.random() * 75)
        for(let i=0; i<reps; ++i){
            console.log('strokin')
            // ctx.strokeStyle = ctx.createConicGradient(Math.floor(Math.random() * 360), Math.floor(Math.random() * 600), Math.floor(Math.random() * 600))
            const colorOpts = '0123456789ABCDEF'
            let curColors = `#${colorOpts[Math.floor(Math.random() * 17)]}${colorOpts[Math.floor(Math.random() * 17)]}${colorOpts[Math.floor(Math.random() * 17)]}`
            curFrame.push(curColors)
            ctx.strokeStyle = curColors
            ctx.beginPath();
            let moveTo = [Math.floor(Math.random() * dispWidth), Math.floor(Math.random() * dispHeight)]
            ctx.moveTo(moveTo[0],moveTo[1]);
            curFrame.push(moveTo)
            let lineTo0 = [Math.floor(Math.random() * dispWidth), Math.floor(Math.random() * dispHeight)]
            curFrame.push(lineTo0)
            let lineTo1 = [Math.floor(Math.random() * dispWidth), Math.floor(Math.random() * dispHeight)]
            curFrame.push(lineTo1)
            let lineTo2 = [Math.floor(Math.random() * dispWidth), Math.floor(Math.random() * dispHeight)]
            curFrame.push(lineTo2)
            let lineTo3 = [Math.floor(Math.random() * dispWidth), Math.floor(Math.random() * dispHeight)]
            curFrame.push(lineTo3)
            let lineTo4 = [Math.floor(Math.random() * dispWidth), Math.floor(Math.random() * dispHeight)]
            curFrame.push(lineTo4)
            let lineTo5 = [Math.floor(Math.random() * dispWidth), Math.floor(Math.random() * dispHeight)]
            curFrame.push(lineTo5)
            let lineTo6 = [Math.floor(Math.random() * dispWidth), Math.floor(Math.random() * dispHeight)]
            curFrame.push(lineTo6)
            ctx.lineTo(lineTo0[0],lineTo0[1])
            ctx.lineTo(lineTo1[0],lineTo1[1])
            ctx.lineTo(lineTo2[0],lineTo2[1])
            ctx.lineTo(lineTo3[0],lineTo3[1])
            ctx.lineTo(lineTo4[0],lineTo4[1])
            ctx.lineTo(lineTo5[0],lineTo5[1])
            ctx.lineTo(lineTo6[0],lineTo6[1])
            ctx.closePath();
            ctx.stroke();
        }
        curFrame.push("end")
        frames=0
    }
}


function checkGenerating(){
    console.log('checking isGenerating')
    if(isGenerating){
        return
    } else {
        generateImg()
    }
}

function startGenerator(){
    // drawOn()
    setInterval(checkGenerating(), 1000)

}
function stopGenerator(){
    clearInterval(checkGenerating)
}